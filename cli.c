/*
 * blu-save helps you to backup your Blu-ray discs
 * Copyright 2019 Yunxiang Li
 *
 * This file is part of blu-save.
 *
 * blu-save is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blu-save is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdio.h>

#include "blu_save.h"

void
show_error (void *error_sink, const char *fmt, const char *arg)
{
  fprintf (error_sink, fmt, arg);
}

int
main (int argc, char *argv[])
{
  char *disc_path = NULL, *out_path = NULL, *key_path = NULL;

  switch (argc)
    {
    case 4:
      key_path = argv[3];
    case 3:
      out_path = argv[2];
    case 2:
      disc_path = argv[1];
      break;
    default:
      show_error (stderr, "Usage: %s <disc_path> [out_path] [key_file]\n",
                  argv[0]);
      return -1;
    }

  bool run = true;
  blu_save_dump_bd (disc_path, out_path, key_path, stderr, &run);

  return 0;
}
