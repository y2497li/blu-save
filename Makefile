# blu-save helps you to backup your Blu-ray discs
# Copyright 2019 Yunxiang Li
#
# This file is part of blu-save.
#
# blu-save is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# blu-save is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with blu-save.  If not, see <https://www.gnu.org/licenses/>.

CXX = gcc
CXXFLAGS = -g -Os -pedantic -Wall
LIBBR_DIR = libbluray
LIBBR_H = -I ${LIBBR_DIR}/src -I ${LIBBR_DIR}/src/libbluray
LIBBR_L = -lxml2 -lfontconfig -lfreetype -ldl -lpthread
GTKLIB_H = `pkg-config --cflags gtk+-3.0`
GTKLIB_L = `pkg-config --libs gtk+-3.0`
VERSION = `git tag | tail -n1`

blu-save : cli.c blu_save.c ${LIBBR_DIR}/.libs/libbluray.a
	${CXX} ${CXXFLAGS} ${LIBBR_H} ${LIBBR_L} $^ -o $@

blu-save-gtk : gtk.c blu_save.c ${LIBBR_DIR}/.libs/libbluray.a
	${CXX} ${CXXFLAGS} -rdynamic ${LIBBR_H} ${GTKLIB_H} ${LIBBR_L} ${GTKLIB_L} $^ -o $@

${LIBBR_DIR}/.libs/libbluray.a : ${LIBBR_DIR}/.git
	./make-lib.sh

.PHONY : clean cli gtk all

clean :
	#-./make-lib.sh clean
	-rm blu-save*

cli : blu-save
gtk : blu-save-gtk
all : cli gtk

release : clean all
	strip blu-save blu-save-gtk
	tar -czvf blu-save-${VERSION}.tar.gz ../blu-save/blu-save ../blu-save/blu-save-gtk ../blu-save/COPYING ../blu-save/README.md
