/*
 * blu-save helps you to backup your Blu-ray discs
 * Copyright 2019 Yunxiang Li
 *
 * This file is part of blu-save.
 *
 * blu-save is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blu-save is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef BLU_SAVE_H
#define BLU_SAVE_H

#include <stdbool.h>

#define BD_NAME_MAX 255
#define BLOCK_SIZE 6144
#define SPEED 60

#define DEFAULT_OUTDIR "out"

/**
 *  Print error message with appropriate means, user must implement
 *
 * @param error_sink  pointer to the appropriate resource (file, window, etc.)
 * @param fmt         format string WITH ONLY ONE ARGUMENT
 * @param arg         argument
 */
void
show_error (void *error_sink, const char *fmt, const char *arg);

/**
 *  Save Blu-ray content, using key to decrypt if necessary
 *
 * @param disc_path   path to mounted Blu-ray disc, device or image file
 * @param out_path    path to save the output to, will create it if needed
 * @param key_path    path to KEYDB.cfg (may be NULL)
 * @param error_sink  error_sink to pass to show_error
 * @param run         set to false to abort the running dump safely
 */
void
blu_save_dump_bd (const char *disc_path, const char *out_path,
                  const char *key_path, void *error_sink, const bool *run);

#endif
