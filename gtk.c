/*
 * blu-save helps you to backup your Blu-ray discs.
 * Copyright 2019 Yunxiang Li
 *
 * This file is part of blu-save.
 *
 * blu-save is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blu-save is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <glib.h>
#include <gtk/gtk.h>

#include "bluray.h"
#include "file/file.h"
#include "blu_save.h"

typedef struct
{
  GtkWidget *window;
  const char *fmt;
  char *arg;
} error_t;

gboolean
_error_popup (gpointer p)
{
  error_t *e = p;
  GtkWidget *dialog = gtk_message_dialog_new (GTK_WINDOW (e->window),
                                              GTK_DIALOG_DESTROY_WITH_PARENT,
                                              GTK_MESSAGE_ERROR,
                                              GTK_BUTTONS_CLOSE, e->fmt,
                                              e->arg);
  gtk_dialog_run (GTK_DIALOG (dialog));
  gtk_widget_destroy (dialog);
  g_free (e->arg);
  g_free (e);
  return 0;
}

void
show_error (void *error_sink, const char *fmt, const char *arg)
{
  error_t *e = g_malloc (sizeof (error_t));
  e->window = error_sink;
  e->fmt = fmt;
  e->arg = g_strdup (arg);
  gdk_threads_add_idle (_error_popup, (gpointer) e);
}

typedef struct
{
  GtkWidget *window;
  GtkWidget *disc_file_w, *disc_dir_w, *out_dir_w, *key_file_w;
  GtkWidget *spinner, *button;

  gulong go_handler_id;
  gulong stop_handler_id;

  gchar *disc_dir, *out_dir, *key_file;
  GThread *worker;
  bool run;
} data_t;

void
set_ui_idle (data_t *d)
{
  gtk_widget_hide (d->spinner);
  gtk_spinner_stop (GTK_SPINNER (d->spinner));

  g_signal_handler_block (GTK_BUTTON (d->button), d->stop_handler_id);
  g_signal_handler_unblock (GTK_BUTTON (d->button), d->go_handler_id);

  gtk_button_set_label (GTK_BUTTON (d->button), "Go");
}

gboolean
check_finished (gpointer p)
{
  data_t *d = p;
  if (d->run == false && d->worker)
    {
      g_thread_join (d->worker);
      d->worker = NULL;
      set_ui_idle (d);
    }
  return d->run;
}

void
on_stop_clicked (GtkWidget *button, data_t *d)
{
  d->run = false;
  check_finished (d);
}

gpointer
dump_bd (gpointer p)
{
  data_t *d = p;

  d->run = true;
  blu_save_dump_bd (d->disc_dir, d->out_dir, d->key_file, d->window, &d->run);
  g_free (d->disc_dir);
  g_free (d->out_dir);
  g_free (d->key_file);
  d->run = false;

  return NULL;
}

void
set_ui_busy (data_t *d)
{
  gtk_spinner_start (GTK_SPINNER (d->spinner));
  gtk_widget_show (d->spinner);

  g_signal_handler_block (GTK_BUTTON (d->button), d->go_handler_id);
  g_signal_handler_unblock (GTK_BUTTON (d->button), d->stop_handler_id);

  gtk_button_set_label (GTK_BUTTON (d->button), "Stop");

  gdk_threads_add_timeout_seconds (1, check_finished, d);
}

gchar *
get_path (void *widget)
{
  return gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (widget));
}

void
on_go_clicked (GtkWidget *button, data_t *d)
{
  if (!((d->disc_dir = get_path (d->disc_file_w))
        || (d->disc_dir = get_path (d->disc_dir_w))))
    return;
  d->out_dir = get_path (d->out_dir_w);
  d->key_file = get_path (d->key_file_w);

  d->worker = g_thread_new (NULL, dump_bd, d);

  set_ui_busy (d);
}

void
on_close (GtkWidget *window, data_t *d)
{
  d->run = false;
  if (d->worker != NULL)
    g_thread_join (d->worker);
  gtk_main_quit ();
}

GtkWidget *
add_window (const char *name, guint width, guint height, guint border)
{
  GtkWidget *window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_window_set_title (GTK_WINDOW (window), name);
  gtk_window_set_default_size (GTK_WINDOW (window), width, height);
  gtk_container_set_border_width (GTK_CONTAINER (window), border);
  return window;
}

GtkWidget *
add_grid (GtkWidget *window, guint spacing)
{
  GtkWidget *grid = gtk_grid_new ();
  gtk_grid_set_row_spacing (GTK_GRID (grid), spacing);
  gtk_grid_set_column_spacing (GTK_GRID (grid), spacing);
  gtk_grid_set_row_homogeneous (GTK_GRID (grid), TRUE);
  gtk_container_add (GTK_CONTAINER (window), GTK_WIDGET (grid));
  return grid;
}

GtkWidget *
add_chooser (const char *name, const char *title, guint action, GtkWidget *grid,
             GtkWidget *prev)
{
  GtkWidget *label = gtk_label_new (name);
  gtk_widget_set_halign (label, GTK_ALIGN_END);

  GtkWidget *chooser = gtk_file_chooser_button_new (title, action);
  gtk_widget_set_hexpand (chooser, TRUE);

  gtk_grid_attach_next_to (GTK_GRID (grid), chooser, prev, GTK_POS_BOTTOM, 1,
                           1);
  gtk_grid_attach_next_to (GTK_GRID (grid), label, chooser, GTK_POS_LEFT, 1, 1);
  return chooser;
}

#define CHOOSE_FILE GTK_FILE_CHOOSER_ACTION_OPEN
#define CHOOSE_FOLDER GTK_FILE_CHOOSER_ACTION_SELECT_FOLDER

int
main ()
{
  data_t d;

  gtk_init (0, NULL);

  d.window = add_window ("blu-save", 512, 0, 8);
  GtkWidget *grid = add_grid (d.window, 8);

  d.disc_file_w = add_chooser ("Read file:", "Disc file", CHOOSE_FILE, grid,
                               NULL);
  d.disc_dir_w = add_chooser ("or folder:", "Disc folder", CHOOSE_FOLDER, grid,
                              d.disc_file_w);
  d.out_dir_w = add_chooser ("Save to:", "Output folder", CHOOSE_FOLDER, grid,
                             d.disc_dir_w);
  d.key_file_w = add_chooser ("Key file:", "KEYDB.cfg (optional)", CHOOSE_FILE,
                              grid, d.out_dir_w);

  d.button = gtk_button_new_with_label ("Go");
  d.spinner = gtk_spinner_new ();
  gtk_grid_attach_next_to (GTK_GRID (grid), d.button, d.key_file_w,
                           GTK_POS_BOTTOM, 1, 1);
  gtk_grid_attach_next_to (GTK_GRID (grid), d.spinner, d.button, GTK_POS_LEFT,
                           1, 1);

  d.go_handler_id = g_signal_connect (GTK_BUTTON (d.button), "clicked",
                                      G_CALLBACK (on_go_clicked), &d);
  d.stop_handler_id = g_signal_connect (GTK_BUTTON (d.button), "clicked",
                                        G_CALLBACK (on_stop_clicked), &d);

  d.worker = NULL;
  d.run = false;

  g_signal_handler_block (GTK_BUTTON (d.button), d.stop_handler_id);
  g_signal_connect (d.window, "destroy", G_CALLBACK (on_close), &d);

  gtk_widget_show_all (d.window);
  gtk_widget_hide (d.spinner);

  gtk_main ();

  return 0;
}
