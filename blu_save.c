/*
 * blu-save helps you to backup your Blu-ray discs
 * Copyright 2019 Yunxiang Li
 *
 * This file is part of blu-save.
 *
 * blu-save is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blu-save is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

#include "bluray.h"
#include "file/file.h"
#include "blu_save.h"

void
dump_file (BLURAY *bd, const char *rel_path, const char *cache_path,
           void *error_sink, const bool *run)
{
  BD_FILE_H *fp_in;
  FILE *fp_out;
  uint8_t buf[BLOCK_SIZE * SPEED];

  // open input file
  if (!(fp_in = bd_open_file_dec (bd, rel_path)))
    {
      show_error (error_sink, "failed to open %s\n", rel_path);
      return;
    }

  // open output file
  if (!(fp_out = fopen (cache_path, "wb")))
    {
      file_close (fp_in);
      show_error (error_sink, "failed to create cache file %s\n", cache_path);
      return;
    }

  // write to output file
  size_t got;
  do
    {
      got = file_read (fp_in, buf, sizeof (buf));

      // we'll call write(fp, buf, 0) after EOF. It is used to check for errors.
      if (got < 0 || fwrite (buf, 1, got, fp_out) != got)
        {
          remove (cache_path);
          file_close (fp_in);
          fclose (fp_out);
          show_error (error_sink, "failed to cache file %s\n", rel_path);
          return;
        }
    }
  while (*run && got > 0);

  file_close (fp_in);
  fclose (fp_out);
}

static size_t
new_path_size (const char *path)
{
  return strlen (path) + strlen (DIR_SEP) + BD_NAME_MAX + 1;
}

void
dump_dir (BLURAY *bd, const char *base_dir, const char *out_dir,
          void *error_sink, const bool *run)
{
  BD_DIRENT ep;
  BD_DIR_H *dp;

  if (!(dp = (BD_DIR_H *) bd_open_dir (bd, base_dir)))
    {
      show_error (error_sink, "failed to open %s\n", base_dir);
      return;
    }

  if (dir_read (dp, &ep) == 0)
    { // non-empty dir
      char *base = malloc (new_path_size (base_dir));
      char *out = malloc (new_path_size (out_dir));

      mkdir (out_dir, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH); // drwxrwxr-x
      do
        {
          char *name = ep.d_name;
          if (*base_dir)
            strcat (strcat (strcpy (base, base_dir), DIR_SEP), name);
          else
            strcpy (base, name);

          if (*out_dir)
            strcat (strcat (strcpy (out, out_dir), DIR_SEP), name);
          else
            strcpy (out, name);

          if (strchr (name, '.'))
            {
              if (strlen (name) > 2)
                dump_file (bd, base, out, error_sink, run);
            }
          else
            {
              dump_dir (bd, base, out, error_sink, run);
            }
        }
      while (*run && dir_read (dp, &ep) == 0);
      free (base);
      free (out);
    }
  dir_close (dp);
}

void
blu_save_dump_bd (const char *disc_path, const char *out_path,
                  const char *key_path, void *error_sink, const bool *run)
{
  BLURAY *bd;
  if (!(bd = bd_open (disc_path, key_path)))
    {
      show_error (error_sink, "failed to open %s\n", disc_path);
      return;
    }
  if (!(out_path && *out_path))
    out_path = DEFAULT_OUTDIR;
  dump_dir (bd, "", out_path, error_sink, run);
  bd_close (bd);
}
